/**
 * Created by Antonio Aranda en 2017
 * Institut Gabriela Mistral
 * Sant Vicenç dels Horts (Barcelona)
 */
package com.antares.micalculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final String cero = "0";
    private final String uno = "1";
    private final String dos = "2";
    private final String tres = "3";
    private final String cuatro = "4";
    private final String cinco = "5";
    private final String seis = "6";
    private final String siete = "7";
    private final String ocho = "8";
    private final String nueve = "9";
    private final String punto = ".";
    private double valor1 = 0.0;

    private enum Operador {MAS, MENOS, POR, DIVIDIDO}

    private Operador operacionPendiente;
    private TextView display;
    private String displayString;
    private boolean nuevaPantalla = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.display);
        display.setText(cero);

        //Tecla 0
        Button button0 = findViewById(R.id.button_0);
        button0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(cero);
            }
        });

        //Tecla 1
        Button button1 = findViewById(R.id.button_1);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(uno);
            }
        });

        //Tecla 2
        Button button2 = findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(dos);
            }
        });

        //Tecla 3
        Button button3 = findViewById(R.id.button_3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(tres);
            }
        });

        //Tecla 4
        Button button4 = findViewById(R.id.button_4);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(cuatro);
            }
        });

        //Tecla 5
        Button button5 = findViewById(R.id.button_5);
        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(cinco);
            }
        });

        //Tecla 6
        Button button6 = findViewById(R.id.button_6);
        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(seis);
            }
        });

        //Tecla 7
        Button button7 = findViewById(R.id.button_7);
        button7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(siete);
            }
        });

        //Tecla 8
        Button button8 = findViewById(R.id.button_8);
        button8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(ocho);
            }
        });

        //Tecla 9
        Button button9 = findViewById(R.id.button_9);
        button9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(nueve);
            }
        });

        //Tecla . decimal
        final Button buttonPunto = findViewById(R.id.button_punto);
        buttonPunto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onDisplay(punto);
                buttonPunto.setOnClickListener(null);
            }
        });

        //Tecla suma (+)
        Button buttonSumar = findViewById(R.id.button_sumar);
        buttonSumar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onOperacion(Operador.MAS);
            }
        });

        //Tecla resta (-)
        Button buttonRestar = findViewById(R.id.button_restar);
        buttonRestar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onOperacion(Operador.MENOS);
            }
        });

        //Tecla multiplicación (*)
        Button buttonMultiplicar = findViewById(R.id.button_multiplicar);
        buttonMultiplicar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onOperacion(Operador.POR);
            }
        });

        //Tecla división (/)
        Button buttonDividir = findViewById(R.id.button_dividir);
        buttonDividir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onOperacion(Operador.DIVIDIDO);
            }
        });

        //Tecla limpiar pantalla (CLR)
        Button buttonClear = findViewById(R.id.button_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                displayString = cero;
                display.setText(displayString);
                valor1 = 0.0;
                operacionPendiente = null;
                nuevaPantalla = false;
            }
        });

        //Tecla resultado (=)
        Button buttonIgual = findViewById(R.id.button_igual);
        buttonIgual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (operacionPendiente != null) {
                    Double valor2 = Double.valueOf(display.getText().toString());
                    Double resultado = 0.0;
                    switch (operacionPendiente) {
                        case MAS:
                            resultado = valor1 + valor2;
                            break;
                        case MENOS:
                            resultado = valor1 - valor2;
                            break;
                        case POR:
                            resultado = valor1 * valor2;
                            break;
                        case DIVIDIDO:
                            resultado = valor1 / valor2;
                            break;
                    }
                    displayString = String.valueOf(resultado);
                    display.setText(displayString);
                    valor1 = 0.0;
                    operacionPendiente = null;
                    nuevaPantalla = true;
                }
            }
        });
    }

    /**
     *
     * @param tecla incorpora en el display el número pulsado
     */
    private void onDisplay(String tecla) {
        displayString = display.getText().toString();
        if (displayString.equals(cero) || nuevaPantalla) {
            displayString = tecla;
            nuevaPantalla = false;
        } else {
            displayString = displayString + tecla;
        }
        display.setText(displayString);
    }

    /**
     *
     * @param operador almacena variables de trabajo cuando se pulsa una tecla de operación
     */
    private void onOperacion(Operador operador) {
        valor1 = Double.valueOf(displayString);
        nuevaPantalla = true;
        operacionPendiente = operador;
    }
}
